# javascript-playground

A playground created with the intention of organize my vanilla javascript studies.

## Projects

- [Recreated Javascript methods](https://github.com/lucasmls/javascript-lab/tree/master/jest-recreated-js-methods)
- [Redux counter](https://github.com/lucasmls/javascript-lab/tree/master/redux-counter)

To run any of the project, you can just open the project folder and follow its instructions. 😊

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
