const every = (arr, func) => (
  function internalEvery(internalArray, counter) {
      const [head, ...tail] = internalArray
      return internalArray.length === 0
        ? true
        : !func(head, counter, arr) ? false : internalEvery(tail, counter +1)
  }
)(arr, 0)

export default every