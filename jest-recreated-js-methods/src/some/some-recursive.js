const some = (arr, func) => (
  function internalSome (internalArr, counter) {
    const [head, ...tail] = internalArr
    return internalArr.length === 0
          ? false
          : func(head, counter, arr) ? true : internalSome(tail, counter +1)
  }
)(arr, 0)

export default some