const isInitialValueUndefined = initialValue => initialValue === undefined

const reduce = (arr, func, initialValue) => {
  const acc = isInitialValueUndefined(initialValue) ? arr[0] : initialValue
  const arrCopy = isInitialValueUndefined(initialValue) ? arr.slice(1) : arr

  return (function internalReduce(internalAcc, internalArray, counter) {
    const [head, ...tail] = internalArray
    const nextAcc = () => func(internalAcc, head, counter, arrCopy)
    
    return internalArray.length === 0
      ? internalAcc
      : internalReduce(nextAcc(), tail, counter + 1)
  })(acc, arrCopy, 0)
}

export default reduce