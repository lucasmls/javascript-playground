import reduce from './reduce-recursive'
import reverse from '../reverse/reverse'

const reduceRight = (arr, ...params) => reduce(reverse(arr), ...params)

export default reduceRight
