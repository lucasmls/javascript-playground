import reduce from './reduce'
import reverse from '../reverse/reverse'

const reduceRight = (arr, ...params) => reduce(reverse(arr), ...params)

export default reduceRight