const filter = (arr, func) => (
  function internalFilter (internalArr, counter) {
    const [head, ...tail] = internalArr
    return internalArr.length === 0 ? [] : (func(head, counter, arr) ? [head] : []).concat(internalFilter(tail, counter +1))
  }
)(arr, 0)

export default filter