const find = (arr, func) => {
  return (function internalFind (internalArr, counter) {
    const [head, ...tail] = internalArr
    return internalArr.length === 0
      ? undefined
      : func(head, counter, arr)
        ? head
        : internalFind(tail, counter +1)
  })(arr, 0)
}

export default find