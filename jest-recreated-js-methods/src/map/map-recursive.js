const map = (arr = [], func = (item) => item ) => (
  function internalMap (internalArray, counter) {
    const [head, ...tail] = internalArray
    return internalArray.length === 0 ? [] : [func(head, counter, arr), ...internalMap(tail, counter + 1)]
  }
)(arr, 0)

export default map