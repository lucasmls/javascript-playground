import { expect } from 'chai'
import map from '../../src/map/map-recursive'

describe('Map', () => {

  it('Map should be a function', () => {
    expect(map).to.be.a('function')
  })

  it('map([8, 9], item => item) should return [8, 9]', () => {
    expect(map([8, 9], item => item)).to.be.deep.equal([8, 9])
  })

  it('map([3, 4], item => item) should return [3, 4]', () => {
    expect(map([3, 4], item => item)).to.be.deep.equal([3, 4])
  })

  it('map([1, 2], item => item +1) should return [2, 3]', () => {
    expect(map([1, 2], item => item +1)).to.be.deep.equal([2, 3])
  })

  it('map([2, 3], item => item +1) should return [2, 3]', () => {
    expect(map([2, 3], item => item +1)).to.be.deep.equal([3, 4])
  })

  it('map([1, 2], (item, index) => index)', () => {
    expect(map([1, 2], (item, index) => index)).to.be.deep.equal([0, 1])
  })

  it('map([1, 2], (item, index, array) => array)', () => {
    expect(map([1, 2], (item, index, array) => array)).to.be.deep.equal([[1, 2], [1, 2]])
  })

  it('map() should return []', () => {
    expect(map()).to.be.deep.equal([])
  })
})