import { expect } from 'chai'
import reverse from '../../src/reverse/reverse'

describe('Reverse', () => {
  it('Reverse should be a function', () => {
    expect(reverse).to.be.a('function')
  })

  it('reverse([1]) should return [1]', () => {
    expect(reverse([1])).to.be.deep.equal([1])
  })

  it('reverse([1, 2]) shoud return [2, 1]', () => {
    expect(reverse([1, 2])).to.be.deep.equal([2, 1])
  })

  it('reverse([1, 2, 6, 7]) should return [7, 6, 2, 1]', () => {
    expect(reverse([1, 2, 6, 7])).to.be.deep.equal([7, 6, 2, 1])
  })
})