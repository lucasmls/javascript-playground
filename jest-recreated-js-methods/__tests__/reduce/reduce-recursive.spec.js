import { expect } from 'chai'
import reduce from '../../src/reduce/reduce-recursive'
import reduceRight from '../../src/reduce/reduce-right-recursive'


describe('Reduce', () => {
  it('reduce should be a function', () => {
    expect(reduce).to.be.a('function')
  })

  it('reduce([1, 2, 3], (acc, item) => (acc + item), 0) should return 6', () => {
    expect(reduce([1, 2, 3], (acc, item) => (acc + item), 0)).to.be.deep.equal(6)
  })

  it('reduce([2, 3, 4], (acc, item) => (acc + item), 0) should return 9', () => {
    expect(reduce([2, 3, 4], (acc, item) => (acc + item), 0)).to.be.deep.equal(9)
  })

  it('reduce([2, 3, 4], (acc, item) => (acc + item)) should return 9', () => {
    expect(reduce([2, 3, 4], (acc, item) => (acc + item))).to.be.deep.equal(9)
  })

  it('reduce([1, 2], (acc, item, index) => (acc + index), 0) should return 1', () => {
    expect(reduce([1, 2], (acc, item, index) => (acc + index), 0)).to.be.deep.equal(1)
  })

  it('reduce([1, 2], (acc, item, index) => { acc[`number${item}`] = item; return acc; }, {}) should be {number1: 1, number2: 2}', () => {
    const value = reduce([1, 2], (acc, item, index) => { acc[`number${item}`] = item; return acc; }, {})
    const expectedValue = { number1: 1, number2: 2 }
    expect(value).to.be.deep.equal(expectedValue)
  })

  it('reduce([1, 2], (acc, item, index, array) => (acc + array[index]), 0) should return 3', () => {
    const value = reduce([1, 2], (acc, item, index, array) => (acc + array[index]), 0)
    const expectedValue = 3
    expect(value).to.be.equal(expectedValue)
  })

  it('reduceRight(["nha", "li", "is", "la"], (acc, item) => acc + item, "") should return "laislinha"', () => {
    const value = reduceRight(['nha', 'li', 'is', 'la'], (acc, item) => acc + item, '')
    const expectedValue = 'laislinha'
    expect(value).to.be.deep.equal(expectedValue)
  })
})