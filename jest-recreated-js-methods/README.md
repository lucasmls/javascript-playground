# js-recreated-methods

In this project, i recreated the methods:

- .every
- .filter
- .find
- .map
- .reduce
- .reduceRight
- .reverse
- .some

that are present in vanilla JS, so that a could truly understand whats going on them (i also recreated them in a recursive way).

## How to run

1. Install the dependencies with **yarn**.
2. Run the application with **yarn test:watch**.

## License

This project is licensed under the MIT License.
