console.assert(
  counter(0, { type: 'INCREMENT' }) === 1,
  'Must return 1'
)

console.assert(
  counter(1, { type: 'INCREMENT' }) === 2,
  'Must return 2'
)

console.assert(
  counter(5, { type: 'DECREMENT' }) === 4,
  'Must return 4'
)

console.assert(
  counter(3, { type: 'SOMETHING' }) === 3,
  'Must return the current state'
)

console.assert(
  counter(undefined, {}) === 0,
  'Must return 0, cause yet there is no state created'
)