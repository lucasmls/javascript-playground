const counter = (state = 0, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return state +1

    case 'DECREMENT':
      return state -1
  }

  return state
}

const $counter = document.querySelector('[data-js="counter"]')
const $increment = document.querySelector('[data-js="INCREMENT"]')
const $decrement = document.querySelector('[data-js="DECREMENT"]')

const createStore = reducer => {
  let state
  let subscriptions = []

  const getState = () => state

  const dispatch = action => {
    state = reducer(state, action)
    subscriptions.forEach(func => func())
  }

  const subscribe = (func) => {
    subscriptions.push(func)
  }

  return {
    getState,
    dispatch,
    subscribe
  }
}
const store = createStore(counter)

store.subscribe(() => $counter.textContent = store.getState())

const handleChange = (e) => {
  const action = e.target.dataset.js
  store.dispatch({ type: action })
}

$increment.addEventListener('click', handleChange)
$decrement.addEventListener('click', handleChange)