# redux-counter

A simple counter, where i started my studies with Redux and the Flux architecture

## How to run

Just open the **index.html** file!

## License

This project is licensed under the MIT License.
